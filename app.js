const builder = require("botbuilder"); // By microsoft.
const restify = require("restify"); // To create a Rest service.
const luisSettings = require("./luis.settings.js"); // Our luis settings.
const model = process.env.LUIS_MODEL;

// Setting up LUIS.
const recognizer = new builder.LuisRecognizer(model);
const dialog = new builder.IntentDialog({
    recognizers: [recognizer]
});

// Setting up the bot.
const connector = new builder.ChatConnector({
    appId: process.env.MICROSOFT_APP_ID,
    appPassword: process.env.MICROSOFT_APP_PASSWORD
});
const bot = new builder.UniversalBot(connector);

// Conversations.
const helloConversation = require('./conversations/hello.js');
const directionsConversation = require('./conversations/directions.js');
const interviewPrepConversation = require('./conversations/interview.prep.js');
const interviewStepsConversation = require('./conversations/interview.steps.js');
const jobDescriptionConversation = require('./conversations/job.description.js');
const emailRecruiterConversation = require('./conversations/email.recruiter.js');
const defaultConversation = require('./conversations/default.js');
const thankYouConversation = require('./conversations/thank.you.js');

// Message service
const messageService = require('./messages/message.service.js');

dialog
    .matches(luisSettings.intents.hello, helloConversation.flow)
    .matches(luisSettings.intents.directions, directionsConversation.flow)
    .matches(luisSettings.intents.interviewPrep, interviewPrepConversation.flow)
    .matches(luisSettings.intents.interviewSteps, interviewStepsConversation.flow)
    .matches(luisSettings.intents.jobDescription, jobDescriptionConversation.flow)
    .matches(luisSettings.intents.emailRecruiter, emailRecruiterConversation.flow)
    .matches(luisSettings.intents.thankyou, thankYouConversation.flow)
    .onDefault(defaultConversation.flow);

bot.dialog('/', dialog);
//Sends greeting message when the bot is first added to a conversation
bot.on('conversationUpdate', message => {
    if (message.membersAdded) {
        message.membersAdded.forEach(identity => {
            if (identity.id === message.address.bot.id) {
                const reply = new builder.Message()
                    .address(message.address)
                    .text(messageService.getInitialMessage());
                bot.send(reply);
            }
        });
    }
});

// Setting up resitfy
var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function () {
    console.log(`${server.name} listening to ${server.url}`);
});
server.post('/api/messages', connector.listen());