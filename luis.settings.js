module.exports = {
    intents: {
        hello: "Hello",
        directions: "Directions",
        interviewPrep: "InterviewPrep",
        jobDescription: "JobDescription",
        interviewSteps: "InterviewSteps",
        emailRecruiter: "EmailRecruiter",
        thankyou: "ThankYou",
        none: "None"
    },
    entities: {
        office: {
            entityName: "Office",
            toronto: "toronto",
            newYork: "new york",
            houston: "houston",
            raleigh: "raleigh",
            london: "london",
            krakow: "krakow",
            wroclaw: "wroclaw"
        },
        interviewStage: {
            entityName: "InterviewStage",
            phone: "phone",
            tech1: "tech1",
            tech2: "tech2",
            final: "final",
            client: "client"
        },
        recruiter: {
            entityName: "Recruiter",
            Keegan: "Keegan",
            Khugan: "Khugan",
            Oliver: "Oliver",
            Matt: "Matt",
            Harshal: "Harshal"
        }
    }
}