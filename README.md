# README #

## What is this repository for? ##

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## How do I get set up? ##

### 1. Install & Run ###

1. Clone this repo
2. `npm install`
3. Add environment variables in notes.txt
    1. To your execution context - or -
    2. To the `configurations.env` in .vscode/launch.json
4. Run
    1. `node app` - or - 
    2. Run/debug from VSCode

// TODO:
* Database configuration
* How to run tests
* Deployment instructions

## Contribution guidelines ##

* Writing tests
* Code review
* Other guidelines

## Who do I talk to? ##

* Harshal Jethwa
* Other community or team contact