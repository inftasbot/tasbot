"use strict";
const builder = require("botbuilder"); // By microsoft.
const messageService = require("../messages/message.service.js");
module.exports = {
    flow: [sayHello]
}

function sayHello(session, args, next) {
    session.sendTyping();
    session.endDialog(messageService.getAskIntentMessage());
}