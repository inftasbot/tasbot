const builder = require("botbuilder"); // By microsoft.
const luisSettings = require("../luis.settings.js");
var request = require('request');
module.exports = {
    flow: [getInterviewStage, firstResponse, secondResponse, mockQuestion, mockQuestionSecond]
}
var interviewStage = ""; // This is to avoid ReferenceError to interviewStage followup.message.
const messages = {
    whichStage: "Interview Prep is important! Do you know which stage you are going to be appearing for?",
    stages: {
        "phone": "Learn about infusion and revise up on general computer science techniques.",
        "tech1": "Look up some OOP concepts and practice small but challenging coding questions.",
        "tech2": "Practice white boarding and brush up on your main technology.",
        "final": "Practice behavioral questions along with computer science fundamentals.",
        "client": "Make sure you have a deep understanding of your primary technology."
    },
    followup: {
        message: `Furthermore, I can help you with the following things for your interview:`,
        options: ["Mock Interview", "Cancel"]
    }
}

function getInterviewStage(session, args, next) {
    session.sendTyping();
    session.dialogData.entities = args.entities;
    var interviewStage = builder.EntityRecognizer.findEntity(args.entities, luisSettings.entities.interviewStage.entityName);
    if (interviewStage) {
        next({
            response: {
                entity: interviewStage.entity
            }
        });
    } else {
        builder.Prompts.choice(session, messages.whichStage, messages.stages, {
            listStyle: builder.ListStyle.button
        });
    }
}

function firstResponse(session, results, next) {
    if (results.response.entity) {
        var interviewStage = results.response.entity; // For string interpolation.
        session.send(messages.stages[results.response.entity]);
        builder.Prompts.choice(session, messages.followup.message, messages.followup.options, {
            listStyle: builder.ListStyle.button
        });
    } else {
        session.next();
    }
}

function secondResponse(session, results, next) {
    if (results.response.entity && results.response.entity === "Mock Interview") {
        builder.Prompts.text(session, "What is inheritance?");
    } else {
        session.endDialog();
    }
}

// Start Mock Interview
function mockQuestion(session, ressults, next) {
    session.sendTyping();
    var sampleAnswer = "Inheritance is one of the feature of Object-Oriented Programming (OOPs). Inheritance allows a class to use the properties and methods of another class. In other words, the derived class inherits the states and behaviors from the base class. The derived class is also called subclass and the base class is also known as super-class. The derived class can add its own additional variables and methods. These additional variable and methods differentiates the derived class from the base class."
    getScore(sampleAnswer, session.message.text, function (score) {
        session.send(score);
        builder.Prompts.text(session, "What is difference between an interface and an abstract class?");
    });
}

function mockQuestionSecond(session, results, next) {
    session.sendTyping();
    var sampleAnswer = "Abstract class can have abstract and non-abstract methods and Interface can have only abstract methods. Abstract class doesn't support multiple inheritance And Interface supports multiple inheritance. Abstract class can have final, non-final, static and non-static variables And Interface has only static and final variables. Abstract class can have static methods, main method and constructor And Interface can't have static methods, main method or constructor. Abstract class can provide the implementation of interface And Interface can't provide the implementation of abstract class. The abstract keyword is used to declare abstract class And The interface keyword is used to declare interface. Abstract class achieves partial abstraction (0 to 100%) because there are non abstract methods also in abstract class whereas interface achieves fully abstraction (100%)."
    getScore(sampleAnswer, session.message.text, function (score) {
        session.send(score);
        session.send("You are ready! Good luck.");
        session.endDialog();
    });
}

function getScore(sampleAnswer, userAnswer, next) {
    var body = "s1=" + sampleAnswer + "&s2=" + userAnswer;
    request({
        url: "https://westus.api.cognitive.microsoft.com/academic/v1.0/similarity",
        method: "POST",
        headers: {
            'Content-Type' : 'application/x-www-form-urlencoded',
            'Ocp-Apim-Subscription-Key': '33e4286a7ce24496bbdf39f00202995c'
        },
        body: body
    }, function (error, response, body) {
        var score = "Score (Out of 100): " +  (parseFloat(response.body, 10) * 100).toFixed(2);
        next(score);
    });
}