"use strict";
const builder = require("botbuilder"); // By microsoft.
const messageService = require("../messages/message.service.js");
module.exports = {
    flow: [sayYoureWelcome]
}

function sayYoureWelcome(session, args, next) {
    session.sendTyping();
    session.endDialog(messageService.getYoureWelcomeMessage());
}