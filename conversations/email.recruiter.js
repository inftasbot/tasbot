'use strict';

const builder = require("botbuilder"); // By microsoft.
const luisSettings = require("../luis.settings.js");
const messageService = require("../messages/message.service.js");
const sendgrid = require('sendgrid')(process.env.SEND_GRID_KEY);
const sgHelper = require('sendgrid').mail;
// const sendgrid = require('sendgrid')("azure_08c2c860be7452545160beda2fa0a4fa@azure.com", "botComp1");

const messages = {
    whichRecruiter: "And who was your recruiter again?",
    recruiters: {
        "Keegan (The Wordsmith) Grimminck": "kgrimminck@infusion.com",
        "Khugan (The Khugs) Shanmugeswaran": "kshanmugeswaran@infusion.com",
        "Oliver (The Wizard) Zsivanov": "ozsivanov@infusion.com",
        "Matt (The Blazer) Steinberg": "msteinberg@infusion.com",
        "Harshal (Pooh Bear) Jethwa": "hjethwa@infusion.com"
    }
}

module.exports = {
    flow: [determineMessage, determineEmail, determineRecruiter, confirmRecruiter, emailRecruiter]
}

var from_email;
var to_email;
var message;

function determineMessage(session, results, next) {
    session.sendTyping();
    builder.Prompts.text(session, messageService.getEmailRecruiterMessage().initial);
}

function determineEmail(session, results, next) {
    message = results.response;
    session.sendTyping();
    builder.Prompts.text(session, "What's your email?");
}

function determineRecruiter(session, results, next) {
    from_email = new sgHelper.Email(results.response);
    session.sendTyping();
    session.dialogData.entities = results.entities;
    var recruiter = builder.EntityRecognizer.findEntity(results.entities, luisSettings.entities.recruiter.entityName);
    if (recruiter) {
        next({
            response: {
                entity: recruiter.entity
            }
        });
    } else {
        builder.Prompts.text(session, messages.whichRecruiter);
    }
}

function confirmRecruiter(session, results, next) {
    session.sendTyping();
    var recruiter;
    for(var r in messages.recruiters){
        if(r.match(results.response)){
            recruiter = r
            break;
        }
    }
    if(recruiter != null){
        next({
            response: {
                entity: recruiter
            }
        });
    } else {
        builder.Prompts.choice(session, "Sorry didn't quite get that, please choose one of these recruiters", messages.recruiters, {
            listStyle: builder.ListStyle.button
        });
    }
}

function emailRecruiter(session, results, next) {

    if (!results.response.entity) {
        session.endDialog();
    }

    to_email = sgHelper.Email(messages.recruiters[results.response.entity]);

    // Send message
    sendEmail();

    session.send(messageService.getEmailRecruiterMessage().sendEmail);
    session.endDialog();
}

function sendEmail() {
    var subject = getSubject();
    var content = new sgHelper.Content("text/plain", message);
    var mail = new sgHelper.Mail(from_email, subject, to_email, content);
    
    // Add user to cc list
    var personalization = new sgHelper.Personalization();
    personalization.addBcc(from_email);
    personalization.addTo(to_email);
    mail.addPersonalization(personalization);

    var request = sendgrid.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: mail.toJSON()
    });

    sendgrid.API(request, function (error, response) {
        console.log(response.statusCode);
        console.log(response.body);
        console.log(response.headers);
    });
}

function getSubject() {
    var today = new Date()
    var curHr = today.getHours()
    var timeOfDay = "";
    
    if (curHr < 12) {
        timeOfDay = "Morning"
    } else if (curHr < 18) {
        timeOfDay = "Afternoon"
    } else {
        timeOfDay = "Evening"
    }
    
    var subject = messageService.getEmailRecruiterMessage().convoAdjective + " " + timeOfDay + " Conversation with Groot";
    
    return subject;
}