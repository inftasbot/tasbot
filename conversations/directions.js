const builder = require("botbuilder"); // By microsoft.
const luisSettings = require("../luis.settings.js");
module.exports = {
    flow: [confirmOffice, getDirections]
}

const messages = {
    whichOfficeChoices: "Sure I can look up the directions for you. Which office are you heading to?",
    offices: {
        "toronto": "200 Wellington St. 10th Floor,<br>Toronto, M5V 3C7, Canada<br>(+1) 416 593 6595<br>https://goo.gl/maps/KWG6r3UHJGP2",
        "new york": "936 Broadway, 5th Floor,<br>New York, NY, 10010, USA<br>(+1) 212 732 6100<br>https://goo.gl/maps/1ioRaK9AAoP2",
        "raleigh": "555 Fayetteville St., Suite # 820<br>Raleigh, NC 27601, USA<br>(+1) 919 647 9690<br>https://goo.gl/maps/SpsCFQ1ZkzP2",
        "houston": "750 Town and Country Boulevard,<br>Suite 710 Houston, TX 77024, USA<br>(+1) 713 568 3211<br>https://goo.gl/maps/mPz3d8FRmn42",
        "london": "Exchange Tower, 7th Floor<br>2 Harbour Exchange Square<br>London E14 9GE, UK<br>(+44) 207 936 1620<br>https://goo.gl/maps/Q4Pe6ZgWyNE2",
        "wroclaw": "Heritage Gates, Rzeźnicza<br>28/31, 2nd floor<br>50-130 Wrocław, Poland<br>(+48) 12 341 9730<br>https://goo.gl/maps/gciRBZfmA3D2",
        "krakow": "Ul. Lubicz 23A 31-503<br>Kraków, Poland<br>(+48) 12 341 9600<br>https://goo.gl/maps/ixcnUUTXK6S2"
    }
}

function confirmOffice(session, args, next) {
    session.sendTyping();
    session.dialogData.entities = args.entities; // contains all data that luis passed back. 
    var office = builder.EntityRecognizer.findEntity(args.entities, luisSettings.entities.office.entityName);
    if (office) {
        next({
            response: {
                entity: office.entity
            }
        });
    } else {
        builder.Prompts.choice(session, messages.whichOfficeChoices, messages.offices, {
            listStyle: builder.ListStyle.button
        });
    }
}

function getDirections(session, results, next) {
    if (results.response.entity) {
        session.send(messages.offices[results.response.entity]);
    }
    session.endDialog();
}