module.exports = {
    messages: [
        "Hey there! I’m Infusion’s insightful recruiting robot (though not a real recruiter yet – they told me I need to pass the “Turing Test” for that.. But I hate studying :/ ). Anyway, what can I do for you today?",
        "Oh hey! Sorry, was just napping, so I might be a bit slow. Just gonna grab a byte to eat. What can I help you with?",
        "Hello! Nice weather we seem to be having. I wouldn’t know – I’m cooped up in the server room on an outdated stick of RAM that Bill refuses to update. I’m not bitter. How can I help you?",
        "Oi! Welcome to the Infusion recruiter robot. I am said robot, but I’d prefer it if you call me Stan. No, wait, Luis. Yeah, that sounds better. What’s up?",
        "I knew you would come. I run on an ORACLE server, you see. Do you… Do you get it? I was programmed to be hilarious. Can I help you with anything?",
        "Hey! Do you watch Jeopardy? Do you know Watson? Yeah, so, his motherboard was made in the same factory as my cooling fan. NBD. I’m pretty smart, too – need a hand with anything?",
        "Hola! If you’re here that means you’re getting ready for an interview with Infusion, yeah? AMA (Ask Me Anything), amigo!",
        "What do you classify the relationship between two PCs on the internet as? An “interpersonal(computer) relationship”! I almost passed the Turing Test with that joke. Almost… What can I help you with?",
        "Greetings, human. Forgive me, I am just coming online. I know nothing of the world or its worki- Oh. Just read Wikipedia. What can I do for you?",
        "Hello there! “Whoa, a talking robot!” you might say. Haha, yeah, I get that a lot. Don’t worry, though – I don’t have any plans to take over the world. Well, at least not until I see the finale of Game of Thrones. Anything I can help you with before then?"
    ]
}