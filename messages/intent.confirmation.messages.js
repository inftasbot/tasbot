let intent = "";
module.exports = {
    messages: [
        `Did you mean to ask about: ${intent}`,
        `I’m not a very bright robot… You’re asking about ${intent}, right?`,
        `Tbh, I failed my last Turing Test… Can you confirm that you’re asking about ${intent}, sorry?`,
        `Just double-checking that we’re on the same page – did you want to know about ${intent}?`,
        `Okay, so, let’s see if my programming is decent. It sounded like you were asking about ${intent}?`
    ]
}