"use strict";

const askIntentMessages = require("./ask.intent.messages.js").messages;
const defaultMessages = require("./default.messages.js").messages;
const youreWelcomeMessages = require("./youre.welcome.message.js").messages;
const initialMessages = require("./initial.messages.js").messages;
const intentConfirmationMessages = require("./intent.confirmation.messages.js").messages;
const emailRecruiterMessages = require("./email.recruiter.messages.js");

class MessageService {

    constructor() {}

    getInitialMessage() {
        return this._getRandomMessage(initialMessages);
    }

    getDefaultMessage() {
        return this._getRandomMessage(defaultMessages);
    }

    getYoureWelcomeMessage() {
        return this._getRandomMessage(youreWelcomeMessages);
    }

    getAskIntentMessage() {
        return this._getRandomMessage(askIntentMessages);
    }

    getIntentConfirmationMessage() {
        return this._getRandomMessage(intentConfirmationMessages);
    }

    getEmailRecruiterMessage() {
        return {
            initial: this._getRandomMessage(emailRecruiterMessages.initial),
            sendEmail: this._getRandomMessage(emailRecruiterMessages.sendEmail),
            convoAdjective: this._getRandomMessage(emailRecruiterMessages.convoAdjectives)
        };
    }

    _getRandomMessage(messages) {
        return messages[Math.floor(Math.random() * messages.length)];
    }
}

module.exports = new MessageService();