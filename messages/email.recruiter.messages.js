module.exports = {
    initial: [
        "Wow, going right to my manager, eh? Fine. What do you want to tell them?",
        "Already giving up on me? I’m computing as fast as I can! What do you want me to put in the email for the recruiter?",
        "Alright, I can send an email to the recruiter for you – what do you want it to say?",
        "I’ll send the recruiters an email for you. What do you want to say in the message?",
        "Okay :( I’ll get smarter so next time I’ll be able to answer all of your questions. What do you want me to send the recruiters?"
    ],
    sendEmail: [
        "I’ve put it in the express email lane. Anything else I can help you with not related to that?",
        "Email is on its way! You’ll hear back from the recruiter directly. Anything else I can do for you?",
        "Just put the postage stamp on when I realized I am integrated into their emails… Oops :$ Just sent. Anything else I can do for you today?"
    ],
    convoAdjectives:[ "Abysmal", "Invigorating", "Bland"]
}