module.exports = {
    messages: [
        "Hmm Not sure what you meant… I tried putting your message through Wolfram Alpha, but it didn’t work either. Here are some options you can choose from:",
        "I didn’t understand that – I’m not a good listener. But I am a good magician. Pick a card – any card!",
        "Sorry, I lack the programming to compute your query. I do have pretty picture cards to show for situations like these, though:",
        "Whoa, that is way too long or complicated for my parsers to, well, parse. They’re poorly written (blame Keegan). Here’s some things I can do:",
        "Pardon me, I didn’t expect to get a query with such big words. I’m only 32-bit, you know. This is what I can do for you:"
    ]
}